---
openapi: 3.0.2
info:
  title: HR
  version: 1.0.0
  description: 員工管理
  license:
    name: Apache 2.0
    url: https://www.apache.org/licenses/LICENSE-2.0
paths:
  /employees:
    summary: 員工資訊
    description: |-
      取得員工清單
      ``` java
      Sysrem.out.println("");
      ```
    get:
      tags:
      - employee
      responses:
        "200":
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/employee'
          description: 員工資料集
        "401":
          $ref: '#/components/responses/errorResponse'
        "403":
          $ref: '#/components/responses/errorResponse'
      deprecated: false
      summary: Get all employee data
      description: "取得所有員工資料, 並以陣列回傳\n``` json\n[\n  {\n    \"id\": \"1\",\n    \"\
        employee_name\": \"Tiger Nixon\",\n    \"employee_salary\": 320800,\n    \"\
        employee_age\": 61,\n    \"profile_image\": \"\"\n  }\n]\n```"
    post:
      requestBody:
        description: 員工資料
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/employee'
        required: true
      tags:
      - employee
      responses:
        "201":
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/employee'
          description: 員工資料建檔結果
        "401":
          $ref: '#/components/responses/errorResponse'
        "400":
          $ref: '#/components/responses/errorResponse'
        "500":
          $ref: '#/components/responses/errorResponse'
      deprecated: false
      summary: Create new record in database
      description: Create new record in database
  /employees/{id}:
    summary: 單一員工
    description: 操作單一員工
    get:
      tags:
      - employee
      responses:
        "200":
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/employee'
          description: 員工資料
        "400":
          $ref: '#/components/responses/errorResponse'
        "404":
          $ref: '#/components/responses/errorResponse'
        "500":
          $ref: '#/components/responses/errorResponse'
      deprecated: false
      summary: Get a single employee data
      description: Get a single employee data
    parameters:
    - examples:
        employeeID:
          value: '"123456"'
      name: id
      description: 員工編號
      schema:
        type: string
      in: path
      required: true
components:
  schemas:
    employee:
      title: Root Type for employee
      description: 員工
      required:
      - employee_age
      - employee_name
      - employee_salary
      - id
      - profile_image
      type: object
      properties:
        id:
          description: 員編
          type: string
        employee_name:
          description: 姓名
          type: string
        employee_salary:
          format: int32
          description: 薪水
          type: integer
        employee_age:
          format: int32
          description: 年齡
          type: integer
        profile_image:
          description: 照片
          type: string
      example:
        id: "1"
        employee_name: Tiger Nixon
        employee_salary: 320800
        employee_age: 61
        profile_image: ""
    errorMessage:
      title: Root Type for errorMessage
      description: 錯誤訊息
      type: object
      properties:
        timestamp:
          format: date-time
          description: 時間戳記
          type: string
        status:
          format: int32
          description: Status code
          type: integer
        error:
          description: 錯誤類型
          type: string
        message:
          description: 錯誤訊息
          type: string
        path:
          description: URI
          type: string
        errors:
          description: 錯誤詳細說明
          type: array
          items:
            type: object
            properties:
              defaultMessage:
                type: string
              objectName:
                type: string
              field:
                type: string
              rejectedValue:
                format: int32
                type: integer
              bindingFailure:
                type: boolean
              code:
                type: string
      example:
        timestamp: 2020-11-23T02:20:45.938+00:00
        status: 401
        error: Unauthorized
        message: ""
        path: /employees
        errors:
        - defaultMessage: 值 需介於 1 到 2 之間
          objectName: createDto
          field: accessLimit
          rejectedValue: 3
          bindingFailure: false
          code: Range
  responses:
    errorResponse:
      content:
        application/json:
          schema:
            $ref: '#/components/schemas/errorMessage'
      description: 錯誤訊息回覆
  securitySchemes:
    JWT:
      scheme: bearer
      type: http
      description: 使用 JWT 的 Token 機制
    APIKEY:
      type: apiKey
      description: APIKEY
      name: apikey
      in: header
security:
- JWT: []
